new Vue({
  el: '#app',
  data: { 
  	headers : [{title: '', desc: ''},
  				{title: 'Trailblazer Plan', desc: 'Unlock A Customer Marketing Plan For Your Business'},
  				{title: 'Teamwork Plan', desc: 'Unlock A Customer Marketing Plan For Your Business'},
  				{title: 'Enterprise Plan', desc: 'Coming Soon'}],
  	planPrices: [50, 89],
  	planUserAmounts: ['1-3', '4-6'],
  	features1: [{title:'Feature 1', plan1:'A Digital Marketing Plan', plan2: 'A Digital Marketing Plan'},
  		{title:'Feature 2', plan1:'A Public Relation Plan', plan2: 'A Public Relation Plan'},
  		{title:'Feature 3', plan1:'A Branding Plan', plan2: 'A Branding Plan'},
  		{title:'Feature 4', plan1:'A Marketing Event Plan', plan2: 'A Marketing Event Plan'},
  		{title:'Feature 1', plan1:'Detailed Marketing Roadmap', plan2: 'Detailed Marketing Roadmap'},],
  	features2: [{title:'Feature 3', plan1:'Marketing Video Tutorials', plan2: 'Marketing Video Tutorials'},{title:'Feature 4', plan1:'DIY Marketing Guides', plan2: 'DIY Marketing Guides'},],
  	features3: [{title:'Feature 5', plan1:'Marketing Management Dashboard', plan2: 'Marketing Management Dashboard'},
  		{title:'Feature 6', plan1:'Activity & Task Reminders', plan2: 'Activity & Task Reminders'},
  		{title:'Feature 7', plan1:'Project Workflow Tools', plan2: 'Project Workflow Tools'},
  		{title:'Feature 8', plan1:'Team Collaboration', plan2: 'Team Collaboration'},
  		{title:'Feature 9', plan1:'Project Timeline', plan2: 'Project Timeline'},
  		{title:'Feature 10', plan1:'Create Custom Actions', plan2: 'Create Custom Actions'},
  		{title:'Feature 11', plan1:'Invite Team Members', plan2: 'Invite Team Members'},
  		{title:'Feature 12', plan1:'Freelancer Management Tools', plan2: 'Freelancer Management Tools'},]
  }
});